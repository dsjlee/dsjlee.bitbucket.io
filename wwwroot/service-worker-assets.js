﻿self.assetsManifest = {
  "assets": [
    {
      "hash": "sha256-BHNhq10T\/9aejlrFEhS\/NYeYS3Djwv\/pqg2xUjQCx\/s=",
      "url": "css\/app.css"
    },
    {
      "hash": "sha256-rldnE7wZYJj3Q43t5v8fg1ojKRwyt0Wtfm+224CacZs=",
      "url": "css\/bootstrap\/bootstrap.min.css"
    },
    {
      "hash": "sha256-xMZ0SaSBYZSHVjFdZTAT\/IjRExRIxSriWcJLcA9nkj0=",
      "url": "css\/bootstrap\/bootstrap.min.css.map"
    },
    {
      "hash": "sha256-jA4J4h\/k76zVxbFKEaWwFKJccmO0voOQ1DbUW+5YNlI=",
      "url": "css\/open-iconic\/FONT-LICENSE"
    },
    {
      "hash": "sha256-BJ\/G+e+y7bQdrYkS2RBTyNfBHpA9IuGaPmf9htub5MQ=",
      "url": "css\/open-iconic\/font\/css\/open-iconic-bootstrap.min.css"
    },
    {
      "hash": "sha256-OK3poGPgzKI2NzNgP07XMbJa3Dz6USoUh\/chSkSvQpc=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.eot"
    },
    {
      "hash": "sha256-sDUtuZAEzWZyv\/U1xl\/9D3ehyU69JE+FvAcu5HQ+\/a0=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.otf"
    },
    {
      "hash": "sha256-+P1oQ5jPzOVJGC52E1oxGXIXxxCyMlqy6A9cNxGYzVk=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.svg"
    },
    {
      "hash": "sha256-p+RP8CV3vRK1YbIkNzq3rPo1jyETPnR07ULb+HVYL8w=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.ttf"
    },
    {
      "hash": "sha256-cZPqVlRJfSNW0KaQ4+UPOXZ\/v\/QzXlejRDwUNdZIofI=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.woff"
    },
    {
      "hash": "sha256-aF5g\/izareSj02F3MPSoTGNbcMBl9nmZKDe04zjU\/ss=",
      "url": "css\/open-iconic\/ICON-LICENSE"
    },
    {
      "hash": "sha256-p\/oxU91iBE+uaDr3kYOyZPuulf4YcPAMNIz6PRA\/tb0=",
      "url": "css\/open-iconic\/README.md"
    },
    {
      "hash": "sha256-qU+KhVPK6oQw3UyjzAHU4xjRmCj3TLZUU\/+39dni9E0=",
      "url": "favicon.ico"
    },
    {
      "hash": "sha256-IpHFcQvs03lh6W54zoeJRG1jW+VnGmwymzxLoXtKX7Y=",
      "url": "icon-512.png"
    },
    {
      "hash": "sha256-9\/uOh8AjJDWKBpJIiRzw\/r14gKiB3xMpSslR7ejrzrQ=",
      "url": "index.html"
    },
    {
      "hash": "sha256-8l\/iTHT+M60mA94AOeXEF1g5OfLHT9xxD5MMxyqbBdE=",
      "url": "manifest.json"
    },
    {
      "hash": "sha256-yzFf+O\/mlH+Q9klUSqXP2kxGKOUFLPxaww8da8fKhGU=",
      "url": "sample-data\/weather.json"
    },
    {
      "hash": "sha256-ER\/Tx93\/gPBXrbFQwGRdcOqGF3me9be1CK8lgcbVZq4=",
      "url": "_framework\/_bin\/MarketNews.dll"
    },
    {
      "hash": "sha256-eYDoB2ii4ByJxp0n6Zarc5suTiqHBNeT0UiRKeb1PA8=",
      "url": "_framework\/_bin\/Microsoft.AspNetCore.Components.dll"
    },
    {
      "hash": "sha256-kM8JsfdQlTgHChZyYc+zy\/fl7+ZjLBcDwD5Y1ers61Q=",
      "url": "_framework\/_bin\/Microsoft.AspNetCore.Components.Web.dll"
    },
    {
      "hash": "sha256-NH5WRpzsPz3X2iVCAu4yfWTwLD1TKA0pDCpx7DAV6PA=",
      "url": "_framework\/_bin\/Microsoft.AspNetCore.Components.WebAssembly.dll"
    },
    {
      "hash": "sha256-jzHgXWAvWMkKIGFjZoT84tbe72E+H7CvTr\/Dryh4QPs=",
      "url": "_framework\/_bin\/Microsoft.Bcl.AsyncInterfaces.dll"
    },
    {
      "hash": "sha256-UzjQWCQfq1s5OARNiwuCLlJZP0BlG0FBpudgeK81GyU=",
      "url": "_framework\/_bin\/Microsoft.Extensions.Configuration.Abstractions.dll"
    },
    {
      "hash": "sha256-fqtWPsjtVqs0DDvz00uYdZE9sSc1LAscnCBQqnOSyao=",
      "url": "_framework\/_bin\/Microsoft.Extensions.Configuration.dll"
    },
    {
      "hash": "sha256-FvVVk0bGkf55uwkvTJ+BEVN+bQpRI49ZLeahW++cbxA=",
      "url": "_framework\/_bin\/Microsoft.Extensions.Configuration.Json.dll"
    },
    {
      "hash": "sha256-+0NQskIvE0j9rZPJAKDlBkNdwd7jAbmWsDpGh\/s0cJc=",
      "url": "_framework\/_bin\/Microsoft.Extensions.DependencyInjection.Abstractions.dll"
    },
    {
      "hash": "sha256-Ba6UOn05HEtM0sYNKKRlwIKxanSdK2sGdYCveDiysdQ=",
      "url": "_framework\/_bin\/Microsoft.Extensions.DependencyInjection.dll"
    },
    {
      "hash": "sha256-R\/IjSzM4yu7ngoe4PHlbT7++S9Kj094SNq3hzcfPZG8=",
      "url": "_framework\/_bin\/Microsoft.Extensions.Logging.Abstractions.dll"
    },
    {
      "hash": "sha256-YJk7oeBnW71kpBLdUd04KgDlon5UaBMAbR8Na7fvvNo=",
      "url": "_framework\/_bin\/Microsoft.Extensions.Logging.dll"
    },
    {
      "hash": "sha256-hBdX8hxtaPPicmHbUbj725\/aBW2tvXcYwv3rvJNDC6I=",
      "url": "_framework\/_bin\/Microsoft.Extensions.Options.dll"
    },
    {
      "hash": "sha256-pq3+nE\/4hBLlUYx7xlnGuzELtngLDQGkkkEvikN1tss=",
      "url": "_framework\/_bin\/Microsoft.Extensions.Primitives.dll"
    },
    {
      "hash": "sha256-GFqkJmxwbf7WMrZ+MCRzaOADzSsVQ9tcMrMt19fsEeo=",
      "url": "_framework\/_bin\/Microsoft.JSInterop.dll"
    },
    {
      "hash": "sha256-UZa5CS19ZrbM6Csnl8CUIQucvmKMc4TughggTxKhx\/I=",
      "url": "_framework\/_bin\/Microsoft.JSInterop.WebAssembly.dll"
    },
    {
      "hash": "sha256-NWNHUuc94cKqi6xWgDpqG7+UcwCQUorkMMH2WMP3Tp8=",
      "url": "_framework\/_bin\/mscorlib.dll"
    },
    {
      "hash": "sha256-7PY3TYDUC0JXkeVybLxlPVOIWQdKBhSgvMTVTRe+5NQ=",
      "url": "_framework\/_bin\/System.Core.dll"
    },
    {
      "hash": "sha256-jnYUgbo+Fiq0f2AYmbb1sK01QGD2ct0R4316\/VZMFIQ=",
      "url": "_framework\/_bin\/System.dll"
    },
    {
      "hash": "sha256-FavBmP3pyGSwoiAMpuXbAJWyah6f9baEI0yPw2l+LxE=",
      "url": "_framework\/_bin\/System.Net.Http.dll"
    },
    {
      "hash": "sha256-IPzRGfvPcDs79U4jV0FbN6pXG10c5qcpBLmLAhhQSVc=",
      "url": "_framework\/_bin\/System.Net.Http.Json.dll"
    },
    {
      "hash": "sha256-DwvJV7FIIEDJS8nSrGYMSGnWDuD0cCTZ\/zhTNK1R+w4=",
      "url": "_framework\/_bin\/System.Net.Http.WebAssemblyHttpHandler.dll"
    },
    {
      "hash": "sha256-MdxKYt6CivCHuy4t7vaaoobuh\/ShzR1winaJ12l8QeQ=",
      "url": "_framework\/_bin\/System.Runtime.CompilerServices.Unsafe.dll"
    },
    {
      "hash": "sha256-NnFJtmq51CBdRjzNVbpyxLUnER8nf0uBjCksb4\/ZJxU=",
      "url": "_framework\/_bin\/System.Text.Encodings.Web.dll"
    },
    {
      "hash": "sha256-EBB1vrDOGNxjnV4eWuKXWRMowTNP3HH3ZWJW1tz+DTA=",
      "url": "_framework\/_bin\/System.Text.Json.dll"
    },
    {
      "hash": "sha256-ZObVRTb+dWNXm588Zcpea7OKuRWGNFYuTfGatEzOaJo=",
      "url": "_framework\/_bin\/WebAssembly.Bindings.dll"
    },
    {
      "hash": "sha256-mPoqx7XczFHBWk3gRNn0hc9ekG1OvkKY4XiKRY5Mj5U=",
      "url": "_framework\/wasm\/dotnet.3.2.0.js"
    },
    {
      "hash": "sha256-3S0qzYaBEKOBXarzVLNzNAFXlwJr6nI3lFlYUpQTPH8=",
      "url": "_framework\/wasm\/dotnet.timezones.dat"
    },
    {
      "hash": "sha256-UC\/3Rm1NkdNdlIrzYARo+dO\/HDlS5mhPxo0IQv7kma8=",
      "url": "_framework\/wasm\/dotnet.wasm"
    },
    {
      "hash": "sha256-oU0moF6IIkna1pMoSD6Ze63s6aHfjCtUflz7xDmENPg=",
      "url": "_framework\/blazor.webassembly.js"
    },
    {
      "hash": "sha256-fUS9pAEXib9vbmNDfHpxxKAM1qmoKxw6MrOT73HHQEA=",
      "url": "_framework\/blazor.boot.json"
    }
  ],
  "version": "47DEQpj8"
};
